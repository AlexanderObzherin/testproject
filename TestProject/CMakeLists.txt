cmake_minimum_required(VERSION 3.10)

#set the project name
project(TestProject)

#specify the C++ standard
set(CMAKE_CXX_STANDARD 17)

#set header files
set(HEADER_FILES
	
)

#set source files
set(SRC_FILES
    TestProject/Source/main.cpp
)

#add the executable
add_executable(TestProject ${HEADER_FILES} ${SRC_FILES})