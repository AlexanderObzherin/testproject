#Build script for Linux platform
#Written by Alexander Obzherin

PROJECT_ROOT_PATH=$(pwd)

#Clean previousle created binaries
rm -rf Build
#Create new build directory and step into it
mkdir Build
cd Build
PATH_TO_BUILD=$(pwd)
#Run root cmake file
cmake ..
#Run make file
make
